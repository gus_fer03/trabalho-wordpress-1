<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kzFLtWr5BskEzRCT6wlKpwIf+0wVun13GGpP+jtsJkD89ZS7OxC0zfW7Iw6BPkRSABq/Pr30Kb1juEjaCfyIlA==');
define('SECURE_AUTH_KEY',  'bRRHWc916LYaZQaQibq743CSFCswUOKOX0SO6I27bIEp2mOKynh9+6c/i/GW824KxM92B65x7C1nFsIF2wuRPQ==');
define('LOGGED_IN_KEY',    '3YSVtMK7gHeT/sN5aZ+sUUNpK0M7Jbc18gAeo/+7NGH+wngADNcsu0TmjavXippHe+wH/Z+1dB6VAiYSRR+6Qg==');
define('NONCE_KEY',        'jpMO153S3EaD21g1rSdoVP4sZROnDzzJiI+nggVgiTiQbz/d64QaNTJqickDk71HW6j3HXPmAPQn5kj3QYlUrA==');
define('AUTH_SALT',        'I3nTIodicNmXHCulpQx6pOo9gXXES3dkpuOWt5mm7WW4gGq/3ebpw45+HItCIOejD+hINX8KqvxQF9DURrvwxA==');
define('SECURE_AUTH_SALT', 'ZHCq3z9J03VO+q+QTlYUfjGYuPsaZinqRZOV8sxggECD2MnLOPaNgYJ6UIu5jtjVEKFeqi3GUxDpsa6YIA3CtA==');
define('LOGGED_IN_SALT',   '4hmPgVstci3+k3s4F5DwpDqNHkugqp148GlSg0U0aLgobXVt+8bIdKzRK8HiiprQspiQu7j1Uf4cUDcrwbejPw==');
define('NONCE_SALT',       'ib/oEs7aIEpl8QSD9xr8hViZ6TuNuGLittDqIjRCkdBY8lQbKKc07hoYdcl7J1tC1p1cuygoSSPlUxg9JyXplg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
